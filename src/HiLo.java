import java.util.Scanner;

public class HiLo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Get ready for the player to use the object
		Scanner scan = new Scanner(System.in);
		
		// create a new random number from 1-100
		int theNumber = (int)(Math.random()*100 +1);
		
		int guess = 0;
		
		while (guess != theNumber ) {
			System.out.println("Guess a number between 1 and 100");
			
			//get the user's guess
			guess = scan.nextInt();
			
			if (guess < theNumber)
				System.out.println(guess + " is too low. Try again.");
			else if (guess > theNumber)
				System.out.println(guess + "is too high. Try again");
			else
				System.out.println(guess + "is correct. You win");
		
		} //end of while loop guessing
		// Just get something else here...
		
	}

}
